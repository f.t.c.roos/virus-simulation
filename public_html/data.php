<?php
    require_once '../db.php';
    
    $sql = "SELECT (select count(*) from covid19ffa) + (select count(*) from covid19md) + (select count(*) from covid19ed) + (select count(*) from ebolaffa) + (select count(*) from ebolamd) + (select count(*) from ebolaed) + (select count(*) from sarsffa) + (select count(*) from sarsmd) + (select count(*) from sarsed) + (select count(*) from spanishfluffa) + (select count(*) from spanishflumd) + (select count(*) from spanishflued) AS databasetotal";
    $result = mysqli_query($conn, $sql);
    $total = mysqli_fetch_object($result);

    $sql = "SELECT AVG(`healthy`) AS healthy FROM `covid19ffa`";
    $result = mysqli_query($conn, $sql);
    $covid19ffahealthy = mysqli_fetch_object($result);
    
    $sql = "SELECT AVG(`recovered`) AS recovered FROM `covid19ffa`";
    $result = mysqli_query($conn, $sql);
    $covid19ffarecovered = mysqli_fetch_object($result);

    $sql = "SELECT AVG(`deceased`) AS deceased FROM `covid19ffa`";
    $result = mysqli_query($conn, $sql);
    $covid19ffadeceased = mysqli_fetch_object($result);

    $sql = "SELECT AVG(`duration`) AS duration FROM `covid19ffa`";
    $result = mysqli_query($conn, $sql);
    $covid19ffaduration = mysqli_fetch_object($result);

    
    $sql = "SELECT AVG(`healthy`) AS healthy FROM `covid19md`";
    $result = mysqli_query($conn, $sql);
    $covid19mdhealthy = mysqli_fetch_object($result);
    
    $sql = "SELECT AVG(`recovered`) AS recovered FROM `covid19md`";
    $result = mysqli_query($conn, $sql);
    $covid19mdrecovered = mysqli_fetch_object($result);

    $sql = "SELECT AVG(`deceased`) AS deceased FROM `covid19md`";
    $result = mysqli_query($conn, $sql);
    $covid19mddeceased = mysqli_fetch_object($result);

    $sql = "SELECT AVG(`duration`) AS duration FROM `covid19md`";
    $result = mysqli_query($conn, $sql);
    $covid19mdduration = mysqli_fetch_object($result);
    

    $sql = "SELECT AVG(`healthy`) AS healthy FROM `covid19ed`";
    $result = mysqli_query($conn, $sql);
    $covid19edhealthy = mysqli_fetch_object($result);
    
    $sql = "SELECT AVG(`recovered`) AS recovered FROM `covid19ed`";
    $result = mysqli_query($conn, $sql);
    $covid19edrecovered = mysqli_fetch_object($result);

    $sql = "SELECT AVG(`deceased`) AS deceased FROM `covid19ed`";
    $result = mysqli_query($conn, $sql);
    $covid19eddeceased = mysqli_fetch_object($result);

    $sql = "SELECT AVG(`duration`) AS duration FROM `covid19ed`";
    $result = mysqli_query($conn, $sql);
    $covid19edduration = mysqli_fetch_object($result);
    

    $sql = "SELECT AVG(`healthy`) AS healthy FROM `ebolaffa`";
    $result = mysqli_query($conn, $sql);
    $ebolaffahealthy = mysqli_fetch_object($result);
    
    $sql = "SELECT AVG(`recovered`) AS recovered FROM `ebolaffa`";
    $result = mysqli_query($conn, $sql);
    $ebolaffarecovered = mysqli_fetch_object($result);

    $sql = "SELECT AVG(`deceased`) AS deceased FROM `ebolaffa`";
    $result = mysqli_query($conn, $sql);
    $ebolaffadeceased = mysqli_fetch_object($result);

    $sql = "SELECT AVG(`duration`) AS duration FROM `ebolaffa`";
    $result = mysqli_query($conn, $sql);
    $ebolaffaduration = mysqli_fetch_object($result);


    $sql = "SELECT AVG(`healthy`) AS healthy FROM `ebolamd`";
    $result = mysqli_query($conn, $sql);
    $ebolamdhealthy = mysqli_fetch_object($result);
    
    $sql = "SELECT AVG(`recovered`) AS recovered FROM `ebolamd`";
    $result = mysqli_query($conn, $sql);
    $ebolamdrecovered = mysqli_fetch_object($result);

    $sql = "SELECT AVG(`deceased`) AS deceased FROM `ebolamd`";
    $result = mysqli_query($conn, $sql);
    $ebolamddeceased = mysqli_fetch_object($result);

    $sql = "SELECT AVG(`duration`) AS duration FROM `ebolamd`";
    $result = mysqli_query($conn, $sql);
    $ebolamdduration = mysqli_fetch_object($result);
    

    $sql = "SELECT AVG(`healthy`) AS healthy FROM `ebolaed`";
    $result = mysqli_query($conn, $sql);
    $ebolaedhealthy = mysqli_fetch_object($result);
    
    $sql = "SELECT AVG(`recovered`) AS recovered FROM `ebolaed`";
    $result = mysqli_query($conn, $sql);
    $ebolaedrecovered = mysqli_fetch_object($result);

    $sql = "SELECT AVG(`deceased`) AS deceased FROM `ebolaed`";
    $result = mysqli_query($conn, $sql);
    $ebolaeddeceased = mysqli_fetch_object($result);

    $sql = "SELECT AVG(`duration`) AS duration FROM `ebolaed`";
    $result = mysqli_query($conn, $sql);
    $ebolaedduration = mysqli_fetch_object($result);

    
    $sql = "SELECT AVG(`healthy`) AS healthy FROM `spanishfluffa`";
    $result = mysqli_query($conn, $sql);
    $spanishfluffahealthy = mysqli_fetch_object($result);
    
    $sql = "SELECT AVG(`recovered`) AS recovered FROM `spanishfluffa`";
    $result = mysqli_query($conn, $sql);
    $spanishfluffarecovered = mysqli_fetch_object($result);

    $sql = "SELECT AVG(`deceased`) AS deceased FROM `spanishfluffa`";
    $result = mysqli_query($conn, $sql);
    $spanishfluffadeceased = mysqli_fetch_object($result);

    $sql = "SELECT AVG(`duration`) AS duration FROM `spanishfluffa`";
    $result = mysqli_query($conn, $sql);
    $spanishfluffaduration = mysqli_fetch_object($result);


    $sql = "SELECT AVG(`healthy`) AS healthy FROM `spanishflumd`";
    $result = mysqli_query($conn, $sql);
    $spanishflumdhealthy = mysqli_fetch_object($result);
    
    $sql = "SELECT AVG(`recovered`) AS recovered FROM `spanishflumd`";
    $result = mysqli_query($conn, $sql);
    $spanishflumdrecovered = mysqli_fetch_object($result);

    $sql = "SELECT AVG(`deceased`) AS deceased FROM `spanishflumd`";
    $result = mysqli_query($conn, $sql);
    $spanishflumddeceased = mysqli_fetch_object($result);

    $sql = "SELECT AVG(`duration`) AS duration FROM `spanishflumd`";
    $result = mysqli_query($conn, $sql);
    $spanishflumdduration = mysqli_fetch_object($result);


    $sql = "SELECT AVG(`healthy`) AS healthy FROM `spanishflued`";
    $result = mysqli_query($conn, $sql);
    $spanishfluedhealthy = mysqli_fetch_object($result);
    
    $sql = "SELECT AVG(`recovered`) AS recovered FROM `spanishflued`";
    $result = mysqli_query($conn, $sql);
    $spanishfluedrecovered = mysqli_fetch_object($result);

    $sql = "SELECT AVG(`deceased`) AS deceased FROM `spanishflued`";
    $result = mysqli_query($conn, $sql);
    $spanishflueddeceased = mysqli_fetch_object($result);

    $sql = "SELECT AVG(`duration`) AS duration FROM `spanishflued`";
    $result = mysqli_query($conn, $sql);
    $spanishfluedduration = mysqli_fetch_object($result);


    $sql = "SELECT AVG(`healthy`) AS healthy FROM `sarsffa`";
    $result = mysqli_query($conn, $sql);
    $sarsffahealthy = mysqli_fetch_object($result);
    
    $sql = "SELECT AVG(`recovered`) AS recovered FROM `sarsffa`";
    $result = mysqli_query($conn, $sql);
    $sarsffarecovered = mysqli_fetch_object($result);

    $sql = "SELECT AVG(`deceased`) AS deceased FROM `sarsffa`";
    $result = mysqli_query($conn, $sql);
    $sarsffadeceased = mysqli_fetch_object($result);

    $sql = "SELECT AVG(`duration`) AS duration FROM `sarsffa`";
    $result = mysqli_query($conn, $sql);
    $sarsffaduration = mysqli_fetch_object($result);


    $sql = "SELECT AVG(`healthy`) AS healthy FROM `sarsmd`";
    $result = mysqli_query($conn, $sql);
    $sarsmdhealthy = mysqli_fetch_object($result);
    
    $sql = "SELECT AVG(`recovered`) AS recovered FROM `sarsmd`";
    $result = mysqli_query($conn, $sql);
    $sarsmdrecovered = mysqli_fetch_object($result);

    $sql = "SELECT AVG(`deceased`) AS deceased FROM `sarsmd`";
    $result = mysqli_query($conn, $sql);
    $sarsmddeceased = mysqli_fetch_object($result);

    $sql = "SELECT AVG(`duration`) AS duration FROM `sarsmd`";
    $result = mysqli_query($conn, $sql);
    $sarsmdduration = mysqli_fetch_object($result);


    $sql = "SELECT AVG(`healthy`) AS healthy FROM `sarsed`";
    $result = mysqli_query($conn, $sql);
    $sarsedhealthy = mysqli_fetch_object($result);
    
    $sql = "SELECT AVG(`recovered`) AS recovered FROM `sarsed`";
    $result = mysqli_query($conn, $sql);
    $sarsedrecovered = mysqli_fetch_object($result);

    $sql = "SELECT AVG(`deceased`) AS deceased FROM `sarsed`";
    $result = mysqli_query($conn, $sql);
    $sarseddeceased = mysqli_fetch_object($result);

    $sql = "SELECT AVG(`duration`) AS duration FROM `sarsed`";
    $result = mysqli_query($conn, $sql);
    $sarsedduration = mysqli_fetch_object($result);

    mysqli_close($conn); 
?>