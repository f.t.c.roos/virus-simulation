
class Simulation {

    constructor(canvasId){
        this.canvas = null;
        this.context = null;
        this.oldTimeStamp = 0;
        this.simulationObjects = [];
        this.resetCounter = 0;

        this.healthyLbl = null;
        this.infectedLbl = null;
        this.curedLbl = null;
        this.deadLbl = null;

        this.healthyAmount = 151;
        this.infectedAmount = 1;
        this.curedAmount = 0;
        this.deceasedAmount = 0;

        this.timeLbl = null;

        this.isPaused = false

        this.selectedDisease = "COVID-19"
        this.selectedState = "Free-for-all"
        this.patientZero = "unknown"

        this.customSpeed = 25
        this.customFatalityRate = 0.5
        this.customDistancingValue = 0.5
        this.customDuration = 10

        this.covid19Speed = 40
        this.ebolaSpeed = 13
        this.sarsSpeed = 11
        this.spanishFluSpeed = 35

        this.covid19FatalityRate = 0.024
        this.ebolaFatalityRate = 0.5
        this.sarsFatalityRate = 0.096
        this.spanishFluFatalityRate = 0.1

        this.defaultDuration = 10

        this.fps = 60

        this.startTime = null

        this.screenRatio = null
        
        this.zeroInfected = false;
        this.resultsSend = false;

        this.loopId = null

        this.pauseStartTime = null
        
        this.init(canvasId);
    }

    init(canvasId) {
        this.canvas = document.getElementById(canvasId);
        this.context = this.canvas.getContext('2d');

        this.healthyLbl = document.getElementById("healthyRate");
        this.infectedLbl = document.getElementById("infectedRate");
        this.curedLbl = document.getElementById("curedRate");
        this.deadLbl = document.getElementById("deathRate");
        this.timeLbl = document.getElementById("timePassed")
        this.seconds = document.getElementById("seconds");        

        this.screenRatio = this.canvas.width / 800.0

        this.createWorld();
    }

    createWorld() {
        for (var i = 0; i < 4; i++) {
            for (var x = (20*this.screenRatio); x < (this.canvas.width - (21*this.screenRatio)); x+= (20*this.screenRatio)) { //relative
                this.simulationObjects.push(this.createCircle(x, i));
            }
        }

        //set first infection
        var randomIndex = Math.floor(Math.random() * this.simulationObjects.length)
        this.simulationObjects[randomIndex].collided = true
        this.simulationObjects[randomIndex].collisionMoment = performance.now()

        this.zeroInfected = false;
        // 500 is the mass of an death object, while the mass of an static object is 10000
        if (this.simulationObjects[randomIndex].mass > 500) {
            this.patientZero = "static"
        } else {
            this.patientZero = "moving"
        }
        
        this.startTime = performance.now()

        this.updateLabels()
        
        // animation frame for the first time
        // The simulationLoop() is a callback
        this.loopId = window.requestAnimationFrame((timeStamp) => {this.simulationLoop(timeStamp)});
    }

    simulationLoop(timeStamp) {
        var secondsPassed = (timeStamp - this.oldTimeStamp) / 1000;
        this.oldTimeStamp = timeStamp;

       if(!this.isPaused) {
            for (var i = 0; i < this.simulationObjects.length; i++) {
                this.simulationObjects[i].update(secondsPassed);
            }

            this.detectCollisions(secondsPassed);

            this.clearCanvas();

            for (var i = 0; i < this.simulationObjects.length; i++) {
                this.simulationObjects[i].draw();
            }

            //also updates amounts for DB
            this.updateLabels();

            var distancing = document.getElementById("activeDistancingTitle").innerHTML

            if (!this.resultsSend & this.infectedLbl.innerHTML == 0 & (distancing=="free-for-all" || distancing=="Moderate distancing" || distancing=="Extensive distancing")){
                this.resultsSend = true;
                this.sendToDatabase();
            }

            this.loopId = window.requestAnimationFrame((timeStamp) => this.simulationLoop(timeStamp));
         }
    }

    sendToDatabase() {
        var tijd = this.timeLbl.innerText
        var distancing = "unknown";
        if (this.selectedState == "Free-for-all"){
            distancing = "ffa"
        } else if (this.selectedState == "Moderate distancing"){
            distancing = "md"
        } else if (this.selectedState == "Extensive distancing"){
            distancing = "ed"
        }

        var disease;
        if (this.selectedDisease == "Spanish flu"){
            disease = "spanishflu";
        } else if (this.selectedDisease == "COVID-19"){
            disease = "covid19";
        } else if (this.selectedDisease == "SARS") {
            disease = "sars"
        } else if (this.selectedDisease == "Ebola"){
            disease = "ebola"
        }

        var bericht = "disease="+disease+"&distancing="+distancing+"&patientZero="+this.patientZero+"&healthy="+this.healthyAmount+"&recovered="+this.curedAmount +"&deceased="+this.deceasedAmount+"&duration="+tijd;
        var ajax = new XMLHttpRequest();
        ajax.onreadystatechange = function () {
            if (ajax.readyState == 4 && ajax.status == 200) {
                
                //console.log("Data send");

            }
        };
        ajax.open("POST", "send.php", true);
        ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        ajax.send(bericht);
        
        //console.log(bericht);
    }

    pauseSimulation() {
        this.isPaused = !this.isPaused
        
        if (this.loopId && this.isPaused) {
            cancelAnimationFrame(this.loopId)
            this.pauseStartTime = performance.now()
        } else {
            this.oldTimeStamp = performance.now()
            this.startTime += (performance.now() - this.pauseStartTime)

            for (var i = 0; i < this.simulationObjects.length; i++) {
                if(this.simulationObjects[i].collisionMoment != null) {
                    this.simulationObjects[i].collisionMoment += (performance.now() - this.pauseStartTime)
                }
            }

            this.loopId = window.requestAnimationFrame((timeStamp) => this.simulationLoop(timeStamp));
        }
    }
    
    updateLabels() {
        this.infectedAmount = 0;
        this.healthyAmount = 0;
        this.curedAmount = 0;
        this.deceasedAmount = 0;
        
        for(var i = 0; i < this.simulationObjects.length; i++){
            if(!this.simulationObjects[i].collided){
                this.healthyAmount++;
            } else if (this.simulationObjects[i].collided && !this.simulationObjects[i].isCured && !this.simulationObjects[i].isDead){
                this.infectedAmount++;
            } else if (this.simulationObjects[i].isCured){
                this.curedAmount++;
            } else if (this.simulationObjects[i].isDead){
                this.deceasedAmount++;
            }
        }
        
        this.healthyLbl.innerText  = this.healthyAmount;
        this.infectedLbl.innerText = this.infectedAmount;
        this.curedLbl.innerText = this.curedAmount;
        this.deadLbl.innerText = this.deceasedAmount;

        if (this.infectedAmount == 0){
            this.zeroInfected = true;
        }
        
        if(this.infectedAmount > 0) {
            var now = performance.now()
            var timeElapsed = Math.floor((now - this.startTime) / 1000)
            var seconds = (timeElapsed == 1) ? " day" : " days"
            this.timeLbl.innerText = timeElapsed
            this.seconds.innerText = seconds
        }
    }

    detectCollisions(secondsPassed) {
        var obj1;
        var obj2;

        for (var i = 0; i < this.simulationObjects.length; i++) {
            this.simulationObjects[i].isColliding = false;
        }

        for (var i = 0; i < this.simulationObjects.length; i++)
        {
            obj1 = this.simulationObjects[i];

            //wall intersection
            if(obj1.x + (obj1.vx*secondsPassed) > this.canvas.width-obj1.radius || obj1.x + (obj1.vx*secondsPassed) < obj1.radius) {
                obj1.vx = -obj1.vx;
            }
            if(obj1.y + (obj1.vy*secondsPassed) > this.canvas.height-obj1.radius || obj1.y + (obj1.vy*secondsPassed) < obj1.radius) {
                obj1.vy = -obj1.vy;
            }

            for (var j = i + 1; j < this.simulationObjects.length; j++)
            {
                obj2 = this.simulationObjects[j];

                if (this.circleIntersect(obj1.x, obj1.y, obj1.radius, obj2.x, obj2.y, obj2.radius)) {
                    obj1.isColliding = true;
                    obj2.isColliding = true;

                    var vCollision = {x: obj2.x - obj1.x, y: obj2.y - obj1.y};
                    var distance = Math.sqrt((obj2.x-obj1.x)*(obj2.x-obj1.x) + (obj2.y-obj1.y)*(obj2.y-obj1.y));
                    var vCollisionNorm = {x: vCollision.x / distance, y: vCollision.y / distance};
                    var vRelativeVelocity = {x: obj1.vx - obj2.vx, y: obj1.vy - obj2.vy};
                    var speed = vRelativeVelocity.x * vCollisionNorm.x + vRelativeVelocity.y * vCollisionNorm.y;

                    if (speed < 0) {
                        break;
                    }

                    var impulse = 2 * speed / (obj1.mass + obj2.mass);
                    obj1.vx -= (impulse * obj2.mass * vCollisionNorm.x);
                    obj1.vy -= (impulse * obj2.mass * vCollisionNorm.y);
                    obj2.vx += (impulse * obj1.mass * vCollisionNorm.x);
                    obj2.vy += (impulse * obj1.mass * vCollisionNorm.y);

                    if(obj1.collided && !obj1.isCured && !obj1.isDead && !obj2.collided) {
                        obj2.collided = true
                        obj2.collisionMoment = performance.now()
                        // this.infectedAmount++; 
                    } else if (obj2.collided && !obj2.isCured && !obj2.isDead && !obj1.collided) {
                        obj1.collided = true
                        obj1.collisionMoment = performance.now()
                        // this.infectedAmount++; 
                    }
                }
            }
        }
    }

    canvasCircleIntersect(x1, y1, r) {
        if(x1 - r < 0 || y1 - r < 0 || x1 + r > this.canvas.width || y1 + r > this.canvas.height) {
            console.log("Bounce")
            return true

        } else {
            return false
        }
    }

    circleIntersect(x1, y1, r1, x2, y2, r2) {
        // distance between two circles
        var squareDistance = (x1-x2)*(x1-x2) + (y1-y2)*(y1-y2);
    
        // When the distance is smaller or equal to the sum
        // of the two radius, the circles touch or overlap
        return squareDistance <= ((r1 + r2) * (r1 + r2))
    }

    clearCanvas() {
        // Clear canvas
        this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
    }

    createCircle(x, yGrade) {
        switch(this.selectedDisease) {//relative
            case "Ebola":
                return new EbolaCircle(this.context, x + this.xVariation(), this.yValue(yGrade), this.moveSpeed(this.ebolaSpeed), this.moveSpeed(this.ebolaSpeed), 1*this.screenRatio, this.selectedState, this.screenRatio, this.ebolaFatalityRate, this.defaultDuration) 
            case "COVID-19":
                return new CoronaCircle(this.context, x + this.xVariation(), this.yValue(yGrade), this.moveSpeed(this.covid19Speed), this.moveSpeed(this.covid19Speed), 1*this.screenRatio, this.selectedState, this.screenRatio, this.covid19FatalityRate, this.defaultDuration) 
            case "Spanish flu":
                return new SpanishFluCircle(this.context, x + this.xVariation(), this.yValue(yGrade), this.moveSpeed(this.spanishFluSpeed), this.moveSpeed(this.spanishFluSpeed), 1*this.screenRatio, this.selectedState, this.screenRatio, this.spanishFluFatalityRate, this.defaultDuration) 
            case "SARS":
                return new SarsCircle(this.context, x + this.xVariation(), this.yValue(yGrade), this.moveSpeed(this.sarsSpeed), this.moveSpeed(this.sarsSpeed), 1*this.screenRatio, this.selectedState, this.screenRatio, this.sarsFatalityRate, this.defaultDuration)
            case "Custom":
                return new CustomCircle(this.context, x + this.xVariation(), this.yValue(yGrade), this.moveSpeed(this.customSpeed), this.moveSpeed(this.customSpeed), 1*this.screenRatio, this.selectedState, this.screenRatio, this.customFatalityRate, this.customDuration, this.customDistancingValue)
        }
    }

    yValue(grade) {
        var borderHeight = ((this.canvas.height / 4) - (22 * this.screenRatio)) //relative
        switch(grade) {
            case 0:
              return Math.floor(Math.random() * borderHeight) + (12 * this.screenRatio)
            case 1:
                return Math.floor(Math.random() * borderHeight) + 0.25*this.canvas.height
            case 2: 
                return Math.floor(Math.random() * borderHeight) + 0.5*this.canvas.height
            case 3:
                return Math.floor(Math.random() * borderHeight) + 0.75*this.canvas.height
            default:
                break;
        }
    }

    xVariation() {
        var sign = Math.random() < 0.5 ? -1 : 1;
        var absoluteVariation = Math.floor(Math.random() * 2.99) //relative
        return (absoluteVariation * (sign) * this.screenRatio)
    }

    moveSpeed(range) { //relative
        var sign = Math.random() < 0.5 ? -1 : 1;
        var absoluteSpeed = Math.floor(Math.random() * range)
        return (absoluteSpeed * (sign) * this.screenRatio)
    }

    resetSimulation() {
        this.zeroInfected = false;
        this.resultsSend = false;
        this.canvas = document.getElementById('simulationCanvas');
        this.context = this.canvas.getContext('2d');
        this.clearCanvas()
        this.simulationObjects = []
        this.screenRatio = this.canvas.width / 800.0; 
        this.createWorld();
    }

    setDisease(name) {
        this.selectedDisease = name
        this.resetSimulation();
    }

    setCustomParameters(speed, fatalityRate, distancing, duration) {
        this.customSpeed = speed
        this.customFatalityRate = fatalityRate
        this.customDistancingValue = distancing
        this.customDuration = duration
    }

    setState(state) {
        this.selectedState = state
        this.resetSimulation()
    }
}
