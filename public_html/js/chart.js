class ChartJS {
    constructor(id, simulationWidth){
        var screenRatio = simulationWidth / 800.0

        var ctx = document.getElementById(id).getContext('2d');

        var totalCount = 0
        for (var i = 0; i < 4; i++) {
            for (var x = (20*screenRatio); x < (simulationWidth - (21*screenRatio)); x+= (20*screenRatio)) { 
                totalCount++
            }
        }

        this.pauseStartTime = null
        this.isPaused = false
        this.loopId = null

        var totalHealthy = totalCount - 1

        this.chart = new Chart(ctx, {
                            // The type of chart we want to create
                            type: 'line',
                            // The data for our dataset
                            data: {
                                labels: ['0'],
                                datasets: [
                                {
                                    label: 'Healthy',
                                    fill: false,
                                    borderColor: '#00B0FF',
                                    backgroundColor: '#00B0FF',
                                    data: [totalHealthy]
                                },
                                {
                                    label: 'Infected',
                                    fill: false,
                                    borderColor: '#EF5350',
                                    backgroundColor: '#EF5350',
                                    data: [1]
                                },
                                {
                                    label: 'Recovered',
                                    fill: false,
                                    borderColor: '#2bbd7e',
                                    backgroundColor: '#2bbd7e',
                                    data: [0]
                                },
                                {
                                    label: 'Deaths',
                                    fill: false,
                                    borderColor: '#37474F',
                                    backgroundColor: '#37474F',
                                    data: [0]
                                }                                                                
                                ]
                            },
                            
                            // Configuration options go here
                            options: {
                                showTooltips: false,
                                responsive: true,
                                // maintainAspectRatio: false,
                                legend: {
                                    display: true,
                                    labels: {
                                        fontSize: (document.getElementsByClassName('mainContainer')[0].offsetWidth * 0.019),
                                    }
                                },
                                scales: {
                                    yAxes: [{
                                      scaleLabel: {
                                        display: (document.getElementsByClassName('mainContainer')[0].offsetWidth > 375),
                                        labelString: 'Number of people'
                                      }
                                    }],
                                    xAxes: [{
                                        scaleLabel: {
                                          display: (document.getElementsByClassName('mainContainer')[0].offsetWidth > 375),
                                          labelString: 'Time (days) *nominal days'
                                        }
                                      }]
                                }        
                            }
        });

        this.time = performance.now();
        var self = this;
        this.loopId = window.requestAnimationFrame((timeStamp) => {this.graphLoop(timeStamp)});
    }

    graphLoop(timeStamp) { 
        if(!this.isPaused) {
            this.addData(timeStamp)
            this.loopId = window.requestAnimationFrame((timeStamp) => {this.graphLoop(timeStamp)});
        }
    }

    pauseChart() {
        this.isPaused = !this.isPaused
        
        if (this.loopId && this.isPaused) {
            cancelAnimationFrame(this.loopId)
            this.pauseStartTime = performance.now()
        } else {
            this.time += (performance.now() - this.pauseStartTime)
            this.loopId = window.requestAnimationFrame((timeStamp) => this.graphLoop(timeStamp));
        }
    }

    addData(timeStamp) {  
        if (document.getElementById('infectedRate').textContent != '0'){

            var timeElapsed = Math.floor((timeStamp - this.time) / 1000);

            var currentHealthy = document.getElementById('healthyRate').textContent;
            this.chart.data.datasets[0].data[timeElapsed] = parseInt(currentHealthy);
            this.chart.data.labels[timeElapsed] = timeElapsed.toString();

            var currentInfected = document.getElementById('infectedRate').textContent;
            this.chart.data.datasets[1].data[timeElapsed] = parseInt(currentInfected);
            this.chart.data.labels[timeElapsed] = timeElapsed.toString();

            var currentCured = document.getElementById('curedRate').textContent;
            this.chart.data.datasets[2].data[timeElapsed] = parseInt(currentCured);
            this.chart.data.labels[timeElapsed] = timeElapsed.toString();
            
            var currentDeath = document.getElementById('deathRate').textContent;        
            this.chart.data.datasets[3].data[timeElapsed] = parseInt(currentDeath);
            this.chart.data.labels[timeElapsed] = timeElapsed.toString();

            this.chart.update();
        }        
    }

    getCurrentChartData() {
        var results = [
            ["Time (days)"].concat(this.chart.data.labels),
            ["Healthy"].concat(this.chart.data.datasets[0].data),
            ["Infected"].concat(this.chart.data.datasets[1].data),
            ["Recovered"].concat(this.chart.data.datasets[2].data),
            ["Deceased"].concat(this.chart.data.datasets[3].data),
            ];

            console.log(results)

        return results
    }
    
    resetChart() {
        this.chart.data.datasets[0].data = [];
        this.chart.data.datasets[1].data = [];
        this.chart.data.datasets[2].data = [];  
        this.chart.data.datasets[3].data = [];

        this.chart.data.labels = ['0'];

        this.time = performance.now();
        this.chart.update();

        this.graphLoop();
    }
}
