window.onload = function (){
    initCanvas();
    window.simulation = new Simulation('simulationCanvas');
    var canvas = document.getElementById('simulationCanvas');
    window.chart = new ChartJS('myChart', canvas.width);

    window.isPaused = false
    window.userPauseActive = false
    window.isCustomState = false
    
    window.previousWidth = window.innerWidth
    
    window.addEventListener('resize', resizeCanvas);
    
    document.addEventListener("visibilitychange", function() {

        if(document.hidden && !window.isPaused) {
            pause(false)
            //document.title = "The simulation is paused! ;)"
        } else if (!document.hidden && window.isPaused && !window.userPauseActive) {
            pause(false) //unpause
            //document.title = "Interactive Virus Simulation | See how viruses spread"
        } 
        
        if (document.hidden) {
            document.title = "Interactive Virus Simulation | The Simulation is ready to go"
        } else {
            document.title = "Interactive Virus Simulation | See how viruses spread"
        }
    }); 
    this.initRange();  
}   

function redraw() {
    if(window.isPaused) {
        pause(false) //unpause
    }
    window.isPaused = false
    window.userPauseActive = false
    window.simulation.resetSimulation();
    window.chart.resetChart();
    simulation.zeroInfeceted = false;
}  

function pause(fromUser) {
    window.simulation.pauseSimulation();
    window.chart.pauseChart();
    window.isPaused = !window.isPaused
    if(window.isPaused) {
        document.getElementById("pause").innerText = "Play";
        window.userPauseActive = fromUser
    } else {
        document.getElementById("pause").innerText = "Pause";
    }
}

function switchCustomizeState() {
    window.isCustomState = !window.isCustomState

    preSetSliderValues()

    if(!window.isCustomState) {
        changeSimulatedDisease(window.isCustomState)
    }

    var distancingTitle = document.getElementById('activeDistancingTitle')
    var diseaseTitle = document.getElementById('activeVirusTitle')

    if(window.isCustomState) {
        var distancingVal = document.getElementById('distancing').value + "%";
        distancingTitle.innerText = distancingVal + " distancing"
        diseaseTitle.innerText = "Custom Viral Disease"
    } else {
        var diseaseSelect = document.getElementById('virus')
        var diseaseName = diseaseSelect.options[diseaseSelect.selectedIndex].text
        var stateSelect = document.getElementById('system')
        var stateName = stateSelect.options[stateSelect.selectedIndex].text
        distancingTitle.innerText = stateName
        diseaseTitle.innerText = diseaseName
    }
}

function changeSimulatedDisease(custom){
    if(window.isPaused) {
        pause(false) //unpause
    }
    window.isPaused = false
    window.userPauseActive = false
    //reset virus canvas
    if(!custom) {
        var diseaseTitle = document.getElementById('activeVirusTitle')
        var diseaseSelect = document.getElementById('virus')
        var diseaseName = diseaseSelect.options[diseaseSelect.selectedIndex].text
        diseaseTitle.innerText = diseaseName
        window.simulation.setDisease(diseaseName)
    } else {
        //get and set custom parameters
        var speed = document.getElementById('speed').value / 2;
        var fatalityRate = document.getElementById('fatality').value / 100;
        var distancingValue = document.getElementById('distancing').value / 100;
        var duration = document.getElementById('duration').value
        window.simulation.setCustomParameters(speed, fatalityRate, distancingValue, duration) //add 1 to duration to match graph and timer
        window.simulation.setDisease("Custom")
    }

    //reset chart
    window.chart.resetChart();
}

function changeState() {
    if(window.isPaused) {
        pause(false) //unpause
    }
    window.isPaused = false
    window.userPauseActive = false
    //reset state for virus spread
    var distancingTitle = document.getElementById('activeDistancingTitle')
    var stateSelect = document.getElementById('system')
    var stateName = stateSelect.options[stateSelect.selectedIndex].text
    distancingTitle.innerText = stateName
    window.simulation.setState(stateName)

    //reset chart
    window.chart.resetChart();
}

function resizeCanvas() {
        if(window.innerWidth != window.previousWidth) {

        window.previousWidth = window.innerWidth

        var canvas = document.getElementById('simulationCanvas');
        var graph = document.getElementById('myChart');
        var canvasDiv = document.getElementById('simulationDiv');
        var graphDiv = document.getElementById('chartDiv');
        
        canvas.width = canvasDiv.offsetWidth * 0.96;
        canvas.height = canvasDiv.offsetWidth * 0.48;   

        graph.width = graphDiv.offsetWidth * 0.80;
        graph.height = graphDiv.offsetWidth * 0.40;

        if (window.isPaused){
            this.pause(false);
        }

        redraw()
    }
}

function initCanvas (){
    var canvas = document.getElementById('simulationCanvas');
    var graph = document.getElementById('myChart');
    var canvasDiv = document.getElementById('simulationDiv');
    var graphDiv = document.getElementById('chartDiv');
    
    canvas.width = canvasDiv.offsetWidth * 0.96;
    canvas.height = canvasDiv.offsetWidth * 0.48;   

    graph.width = graphDiv.offsetWidth * 0.80;
    graph.height = graphDiv.offsetWidth * 0.40;
}

var currentValue = 0;
function handleRadioClick(myRadio){
    var select = document.getElementById('selectBoxxes')
    var sliders = document.getElementById('sliders')

    if(myRadio.value == "predefined") {
        select.style.display = "block";
        sliders.style.display = "none";
    } else {
        select.style.display = "none";
        sliders.style.display = "block";
    }
    switchCustomizeState()
}

function preSetSliderValues() {
    if(window.isCustomState) {
        switch(window.simulation.selectedState) {
            case "Free-for-all":
                document.getElementById('distancing').value = 0;
                break;
            case "Moderate distancing":
                document.getElementById('distancing').value = 25;
                break;
            case "Extensive distancing":
                document.getElementById('distancing').value = 50;
                break;
            default:
                return
        }

        switch(window.simulation.selectedDisease) {
            case "Ebola":
                document.getElementById('speed').value = window.simulation.ebolaSpeed*2;
                document.getElementById('fatality').value = window.simulation.ebolaFatalityRate * 100;
                document.getElementById('duration').value = window.simulation.defaultDuration

                break;
            case "COVID-19":
                document.getElementById('speed').value = window.simulation.covid19Speed*2;
                document.getElementById('fatality').value = window.simulation.covid19FatalityRate * 100;
                document.getElementById('duration').value = window.simulation.defaultDuration
                break;
            case "SARS":
                document.getElementById('speed').value = window.simulation.sarsSpeed*2;
                document.getElementById('fatality').value = window.simulation.sarsFatalityRate * 100;
                document.getElementById('duration').value = window.simulation.defaultDuration
                break;
            case "Spanish flu":
                document.getElementById('speed').value = window.simulation.spanishFluSpeed*2;
                document.getElementById('fatality').value = window.simulation.spanishFluFatalityRate * 100;
                document.getElementById('duration').value = window.simulation.defaultDuration
                break;
            default:
                return
        }

        var speedSlider = document.getElementById('speed');
        var speedOutput = document.getElementById('rangeValueSpeed');
        var fatalitySlider = document.getElementById('fatality');
        var fatalityOutput = document.getElementById('rangeValueFatality');
        var distancingSlider = document.getElementById('distancing');
        var distancingOutput = document.getElementById('rangeValueDistancing');
        var durationSlider = document.getElementById('duration');
        var durationOutput = document.getElementById('rangeValueDuration');
        var reproductionOutput = document.getElementById('rangeValueReproduction')

        speedOutput.innerHTML = speedSlider.value + ' (movements per day)';
        fatalityOutput.innerHTML = fatalitySlider.value + "%";
        distancingOutput.innerHTML = distancingSlider.value + "%";
        durationOutput.innerHTML = durationSlider.value + " days"
        reproductionOutput.innerHTML = calcReproductionNumber(speedSlider.value, distancingSlider.value, durationSlider.value);
    }
}


function initRange() {
    var speedSlider = document.getElementById('speed');
    var speedOutput = document.getElementById('rangeValueSpeed');
    var fatalitySlider = document.getElementById('fatality');
    var fatalityOutput = document.getElementById('rangeValueFatality');
    var distancingSlider = document.getElementById('distancing');
    var distancingOutput = document.getElementById('rangeValueDistancing');
    var durationSlider = document.getElementById('duration');
    var durationOutput = document.getElementById('rangeValueDuration');
    var reproductionOutput = document.getElementById('rangeValueReproduction')


    speedOutput.innerHTML = speedSlider.value + ' (movements per day)';
    fatalityOutput.innerHTML = fatalitySlider.value + "%";
    distancingOutput.innerHTML = distancingSlider.value + "%";
    durationOutput.innerHTML = durationSlider.value + " days"
    reproductionOutput.innerHTML = calcReproductionNumber(speedSlider.value, distancingSlider.value, durationSlider.value);


    speedSlider.oninput = function() {
        speedOutput.innerHTML = this.value + ' (movements per day)';
        reproductionOutput.innerHTML = calcReproductionNumber(speedSlider.value, distancingSlider.value, durationSlider.value);
    }     
    fatalitySlider.oninput = function() {
        fatalityOutput.innerHTML = this.value + "%";
    }
    distancingSlider.oninput = function() {
        distancingOutput.innerHTML = this.value + "%";
        reproductionOutput.innerHTML = calcReproductionNumber(speedSlider.value, distancingSlider.value, durationSlider.value);
    }
    durationSlider.oninput = function() {
        durationOutput.innerHTML = this.value + " days";
        reproductionOutput.innerHTML = calcReproductionNumber(speedSlider.value, distancingSlider.value, durationSlider.value);
    }

    speedSlider.ontouchend = function() {
        changeSimulatedDisease(window.isCustomState)
    }     
    fatalitySlider.ontouchend = function() {
        changeSimulatedDisease(window.isCustomState)
    }
    distancingSlider.ontouchend = function() {
        changeSimulatedDisease(window.isCustomState)

        if(window.isCustomState) {
            var distancingTitle = document.getElementById('activeDistancingTitle')
            var distancingVal = distancingSlider.value + "%";
            distancingTitle.innerText = distancingVal + " distancing"
        }
    }
    durationSlider.ontouchend = function() {
        changeSimulatedDisease(window.isCustomState)
    }

    speedSlider.onmouseup = function() {
        changeSimulatedDisease(window.isCustomState)
    }     
    fatalitySlider.onmouseup = function() {
        changeSimulatedDisease(window.isCustomState)
    }
    distancingSlider.onmouseup = function() {
        changeSimulatedDisease(window.isCustomState)

        if(window.isCustomState) {
            var distancingTitle = document.getElementById('activeDistancingTitle')
            var distancingVal = distancingSlider.value + "%";
            distancingTitle.innerText = distancingVal + " distancing"
        }
    }
    durationSlider.onmouseup = function() {
        changeSimulatedDisease(window.isCustomState)
    }
}

function calcReproductionNumber(speed, distancing, duration) { 
    return ((speed * 0.01) * (1 - distancing * 0.01) * duration * 0.5).toLocaleString('fullwide', {maximumFractionDigits:2})
}


function exportCsvData() {
    var results = window.chart.getCurrentChartData()

    var csvString = "";
    for(var i = 0; i < results[0].length; i++) {
        for(var j = 0; j < results.length; j++) {
            csvString += results[j][i] + ','
        }
        csvString = csvString.substring(0, csvString.length - 1);
        csvString += "\r\n";
    }

    csvString = "data:application/csv," + encodeURIComponent(csvString);
    var x = document.createElement("A");
    x.setAttribute("href", csvString );
    x.setAttribute("download","simulationData.csv");
    document.body.appendChild(x);
    x.click();
}