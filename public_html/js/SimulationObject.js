class SimulationObject
{
    constructor (context, x, y, vx, vy, mass, moveState = "Free-for-all", ratio, fatalityRate, duration, distancingValue = null, collided = false, collisionMoment = null, isCured = false, isDead = false) {
        this.context = context;
        this.x = x;
        this.y = y;
        this.vx = vx;
        this.vy = vy;
        this.mass = mass;
        this.ratio = ratio
        this.fatalityRate = fatalityRate
        this.duration = duration
        this.distancingValue = distancingValue
        this.collided = collided;
        this.collisionMoment = collisionMoment;
        this.isCured = isCured
        this.isDead = isDead

        this.moveState = moveState

        this.isColliding = false;

        this.healthyColor = "#00B0FF"
        this.infectedColor = "#EF5350"
        this.curedColor = "#2bbd7e"
        this.deadColor = "#37474F"

        this.applyState()
    }

    applyState() {
        if(this.distancingValue != null) {
            if(Math.random() <= this.distancingValue) {
                this.mass = 10000
                this.vx = 0
                this.vy = 0
            }
            return
        }

        switch(this.moveState) {
            case "Moderate distancing":
                if(Math.random() <= 1 / 4) {
                    this.mass = 10000
                    this.vx = 0
                    this.vy = 0
                }
                break;
            case "Extensive distancing":
                if(Math.random() <= 1 / 2) {
                    this.mass = 10000
                    this.vx = 0
                    this.vy = 0
                }
                break;
            default:
                return
        }
    }
}