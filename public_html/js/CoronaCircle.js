class CoronaCircle extends SimulationObject
{
    constructor (context, x, y, vx, vy, mass, moveState, ratio, fatalityRate, duration, distancingValue, collided, collisionMoment, isCured, isDead){
        super(context, x, y, vx, vy, mass, moveState, ratio, fatalityRate, duration, distancingValue, collided, collisionMoment, isCured, isDead);

        //default 5
        this.radius = 5*ratio;

        this.timer = null
        this.timerCount = 0
    }

    draw(){
        var sickColor = this.isCured ? this.curedColor: (this.isDead ? this.deadColor: this.infectedColor);
        this.context.beginPath();
        this.context.arc(this.x, this.y, this.radius, 0, 2 * Math.PI);
        this.context.fillStyle = this.collided ? sickColor : this.healthyColor ;
        this.context.fill();
    }

    update(secondsPassed){
        //Move with velocity
        this.x += this.vx * secondsPassed;
        this.y += this.vy * secondsPassed;

        this.statusCheck()
    }

    statusCheck() {
        if(this.collisionMoment && this.collided && !this.isCured && !this.isDead) { //infection has occurred 
            var timeElapsed = Math.floor((performance.now() - this.collisionMoment) / 1000)

            if(timeElapsed > this.duration) {
                if(Math.random() > this.fatalityRate) {
                    this.isCured = true
                } else {
                    this.isDead = true 
                    this.mass = 500 //make very heavy so it does not move
                    this.vx = 0
                    this.vy = 0
                }
            }
        }
    }
}