<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Interactive Virus Simulation | Results</title>
    <meta name="description" content="An interactive simulation that shows the spread of viruses like Covid-19 and Ebola. See the impact of different viruses and measures like social distancing"/>
    <link rel="stylesheet" href="css/style.css?v=15">
    <link rel="icon" href="img/virus.png">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<body>
    <?php require_once 'data.php'; ?>
    <header id="topnav">
        <h1 id="pageTitle">
            <a href="https://simulationvirus.com/" class="nostyle" >
                <strong>Virus Simulation</strong>
            </a>
        </h1>
        <h2 id="subTitle">
            <a href="https://simulationvirus.com/" class="nostyle">
                <small>Virus transmission and effect of social distancing</small>
            </a>
        </h2>
    </header>

    <div id="topContent">
        <div id="containerOne">
            <div id="info" class="customCard card">
                <h3 class="cardTitle">INFO</h3>
                <p>
                This is the result page where we show you the average results of all the users. 
                The simulation is a complex system and every simulation run is unique, just as in real life. 
                Combining scenarios is important if we want to say something about this kind of uncertainties. 
                We will not cover the custom simulations, but we do cover the predefined simulations. 
                In total there are 12 possible combinations, because the simulations gives four viruses and three types of social distancing measures. 
                Firstly, we will show you the insight about the effect of social distancing on the duration of an outbreak.
                <br><br>
                The table contains data that is already shown in the bigger table, but now you can clearly see a pattern. 
                The viral disease of COVID-19 and the Spanish flu are seen as quickly spreading viruses, while Ebola and SARS are seen as slowly spreading viruses. 
                The reason for this not important now, but it is good to understand that the slowly spreading viruses are more deadly per case. 
                The average duration of the outbreaks for the viral diseases of COVID-19 and Spanish flu increases, while applying more social distancing. 
                On the other side you see that the average duration for the outbreaks for Ebola and SARS decreases, while applying more social distancing. 
                A reason for this is that the slowly spreading viruses do not manage to reach other entities and will be labeled as local outbreaks. 
                Quickly spreading viruses will more often manage to find a way to spread widely, but do however get slowed down. The conclusion? 
                It is not the question whether social distancing slows down the spread of the coronavirus, but the question is if we care about time or more about death count!
                </p>
            </div>
            <div class="customCard card social-bar">
                <div class="facebook-button">
                    <a target="_blank" href="https://facebook.com/sharer/sharer.php?u=https://simulationvirus.com&t=Share our simulation!"><i class="fa fa-facebook"></i><span class="share-text">Share</span></a>
                </div>
                <div class="insta-button">
                    <a target="_blank" href="https://www.instagram.com/?url=https://www.simulationvirus.com"><i class="fa fa-instagram"></i><span class="share-text">Share</span></a>
                </div>
                <div class="twitter-button">
                    <a target="_blank" href="https://twitter.com/share?text=Share our simulation!&url=https://simulationvirus.com"><i class="fa fa-twitter"></i><span class="share-text">Share</span></a>
                </div>
                <div class="linkedin-button">
                    <a target="_blank" href="https://linkedin.com/shareArticle?url=https://simulationvirus.com&title=Share our simulation!"><i class="fa fa-linkedin"></i><span class="share-text">Share</span></a>
                </div>
            </div>  
        </div>
        
        <div id="averageContainer" class="card">
            <h2>Average results of <?php echo $total->databasetotal ?> simulation runs</h2>

            <h3 class="cardTitle">COVID-19</h3>
            <table>
                <tr>
                    <th>Distancing</th> 
                    <th>Healthy</th>
                    <th>Recovered</th>
                    <th>Deceased</th>
                    <th>Duration</th>
                </tr>
                <tr>
                    <td>Free-for-all</td>
                    <td>
                        <?php 
                            require_once 'data.php';
                            echo round($covid19ffahealthy->healthy);
                        ?>
                    </td>
                    <td>
                        <?php 
                            echo round($covid19ffarecovered->recovered);
                        ?>
                    </td>
                    <td>
                        <?php 
                            echo round($covid19ffadeceased->deceased);
                        ?>
                    </td>
                    <td>
                        <?php 
                            echo round($covid19ffaduration->duration);
                        ?>
                    </td>
                </tr>
                <tr>
                    <td>Moderate distancing</td>
                    <td>
                        <?php 
                            echo round($covid19mdhealthy->healthy);
                        ?>
                    </td>
                    <td>
                        <?php 
                            echo round($covid19mdrecovered->recovered);
                        ?>
                    </td>
                    <td>
                        <?php 
                            echo round($covid19mddeceased->deceased);
                        ?>
                    </td>
                    <td>
                        <?php 
                            echo round($covid19mdduration->duration);
                        ?>
                    </td>
                </tr>
                <tr>
                    <td>Extensive distancing</td>
                    <td>
                        <?php 
                            echo round($covid19edhealthy->healthy);
                        ?>
                    </td>
                    <td>
                        <?php 
                            echo round($covid19edrecovered->recovered);
                        ?>
                    </td>
                    <td>
                        <?php 
                            echo round($covid19eddeceased->deceased);
                        ?>
                    </td>
                    <td>
                        <?php 
                            echo round($covid19edduration->duration);
                        ?>
                    </td>
                </tr>
            </table>
            <br>

            <h3 class="cardTitle">Ebola</h3>
            <table>
                <tr>
                    <th>Distancing</th> 
                    <th>Healthy</th>
                    <th>Recovered</th>
                    <th>Deceased</th>
                    <th>Duration</th>
                </tr>
                <tr>
                    <td>Free-for-all</td>
                    <td>
                        <?php 
                            echo round($ebolaffahealthy->healthy);
                        ?>
                    </td>
                    <td>
                        <?php 
                            echo round($ebolaffarecovered->recovered);
                        ?>
                    </td>
                    <td>
                        <?php 
                            echo round($ebolaffadeceased->deceased);
                        ?>
                    </td>
                    <td>
                        <?php 
                            echo round($ebolaffaduration->duration);
                        ?>
                    </td>
                </tr>
                <tr>
                    <td>Moderate distancing</td>
                    <td>
                        <?php 
                            echo round($ebolamdhealthy->healthy);
                        ?>
                    </td>
                    <td>
                        <?php 
                            echo round($ebolamdrecovered->recovered);
                        ?>
                    </td>
                    <td>
                        <?php 
                            echo round($ebolamddeceased->deceased);
                        ?>
                    </td>
                    <td>
                        <?php 
                            echo round($ebolamdduration->duration);
                        ?>
                    </td>
                </tr>
                <tr>
                    <td>Extensive distancing</td>
                    <td>
                        <?php 
                            echo round($ebolaedhealthy->healthy);
                        ?>
                    </td>
                    <td>
                        <?php 
                            echo round($ebolaedrecovered->recovered);
                        ?>
                    </td>
                    <td>
                        <?php 
                            echo round($ebolaeddeceased->deceased);
                        ?>
                    </td>
                    <td>
                        <?php 
                            echo round($ebolaedduration->duration);
                        ?>
                    </td>
                </tr>
            </table>
            <br>

            <h3 class="cardTitle">Spanish flu</h3>
            <table>
                <tr>
                    <th>Distancing</th> 
                    <th>Healthy</th>
                    <th>Recovered</th>
                    <th>Deceased</th>
                    <th>Duration</th>
                </tr>
                <tr>
                    <td>Free-for-all</td>
                    <td>
                        <?php 
                            echo round($spanishfluffahealthy->healthy);
                        ?>
                    </td>
                    <td>
                        <?php 
                            echo round($spanishfluffarecovered->recovered);
                        ?>
                    </td>
                    <td>
                        <?php 
                            echo round($spanishfluffadeceased->deceased);
                        ?>
                    </td>
                    <td>
                        <?php 
                            echo round($spanishfluffaduration->duration);
                        ?>
                    </td>
                </tr>
                <tr>
                    <td>Moderate distancing</td>
                    <td>
                        <?php 
                            echo round($spanishflumdhealthy->healthy);
                        ?>
                    </td>
                    <td>
                        <?php 
                            echo round($spanishflumdrecovered->recovered);
                        ?>
                    </td>
                    <td>
                        <?php 
                            echo round($spanishflumddeceased->deceased);
                        ?>
                    </td>
                    <td>
                        <?php 
                            echo round($spanishflumdduration->duration);
                        ?>
                    </td>
                </tr>
                <tr>
                    <td>Extensive distancing</td>
                    <td>
                        <?php 
                            echo round($spanishfluedhealthy->healthy);
                        ?>
                    </td>
                    <td>
                        <?php 
                            echo round($spanishfluedrecovered->recovered);
                        ?>
                    </td>
                    <td>
                        <?php 
                            echo round($spanishflueddeceased->deceased);
                        ?>
                    </td>
                    <td>
                        <?php 
                            echo round($spanishfluedduration->duration);
                        ?>
                    </td>
                </tr>
            </table>
            <br>

            <h3 class="cardTitle">SARS</h3>
            <table>
                <tr>
                    <th>Distancing</th> 
                    <th>Healthy</th>
                    <th>Recovered</th>
                    <th>Deceased</th>
                    <th>Duration</th>
                </tr>
                <tr>
                    <td>Free-for-all</td>
                    <td>
                        <?php 
                            echo round($sarsffahealthy->healthy);
                        ?>
                    </td>
                    <td>
                        <?php 
                            echo round($sarsffarecovered->recovered);
                        ?>
                    </td>
                    <td>
                        <?php 
                            echo round($sarsffadeceased->deceased);
                        ?>
                    </td>
                    <td>
                        <?php 
                            echo round($sarsffaduration->duration);
                        ?>
                    </td>
                </tr>
                <tr>
                    <td>Moderate distancing</td>
                    <td>
                        <?php 
                            echo round($sarsmdhealthy->healthy);
                        ?>
                    </td>
                    <td>
                        <?php 
                            echo round($sarsmdrecovered->recovered);
                        ?>
                    </td>
                    <td>
                        <?php 
                            echo round($sarsmddeceased->deceased);
                        ?>
                    </td>
                    <td>
                        <?php 
                            echo round($sarsmdduration->duration);
                        ?>
                    </td>
                </tr>
                <tr>
                    <td>Extensive distancing</td>
                    <td>
                        <?php 
                            echo round($sarsedhealthy->healthy);
                        ?>
                    </td>
                    <td>
                        <?php 
                            echo round($sarsedrecovered->recovered);
                        ?>
                    </td>
                    <td>
                        <?php 
                            echo round($sarseddeceased->deceased);
                        ?>
                    </td>
                    <td>
                        <?php 
                            echo round($sarsedduration->duration);
                        ?>
                    </td>
                </tr>
            </table>
        </div>
    </div>

    <div id="bottomContent">
        <div class="customCardTwee card">
            <h3 class="cardTitle">The effect of social distancing on the duration of an outbreak</h3>
            <table>
                <tr>
                    <th>Distancing</th> 
                    <th>COVID-19</th>
                    <th>Ebola</th>
                    <th>Spanish flu</th>
                    <th>SARS</th>
                </tr>
                <tr>
                    <td>Free-for-all</td>
                    <td>
                        <?php 
                            echo round($covid19ffaduration->duration);
                        ?>
                    </td>
                    <td>
                        <?php 
                            echo round($ebolaffaduration->duration);
                        ?>
                    </td>
                    <td>
                        <?php 
                            echo round($spanishfluffaduration->duration);
                        ?>
                    </td>
                    <td>
                        <?php 
                            echo round($sarsffaduration->duration);
                        ?>
                    </td>
                </tr>
                <tr>
                    <td>Moderate distancing</td>
                    <td>
                        <?php 
                            echo round($covid19mdduration->duration);
                        ?>
                    </td>
                    <td>
                        <?php 
                            echo round($ebolamdduration->duration);
                        ?>
                    </td>
                    <td>
                        <?php 
                            echo round($spanishflumdduration->duration);
                        ?>
                    </td>
                    <td>
                        <?php 
                            echo round($sarsmdduration->duration);
                        ?>
                    </td>
                </tr>
                <tr>
                    <td>Extensive distancing</td>
                    <td>
                        <?php 
                            echo round($covid19edduration->duration);
                        ?>
                    </td>
                    <td>
                        <?php 
                            echo round($ebolaedduration->duration);
                        ?>
                    </td>
                    <td>
                        <?php 
                            echo round($spanishfluedduration->duration);
                        ?>
                    </td>
                    <td>
                        <?php 
                            echo round($sarsedduration->duration);
                        ?>
                    </td>
                </tr>
            </table>
        </div>
    </div>

    <footer class="site-footer">
        <div class="containerFooterOne">
            <div class="aboutUs">
              <h6>About us</h6>
              <p class="footerLbl">
                Our names are Harrison & Floris and we are two Computer Science students at Utrecht University. We have written a research paper that used our simulation to gather results, check it out: <a class="footerLink" href="https://simulationvirus.com/pdf/paperSpreadEbola.pdf">The spread of Ebola virus disease compared to other diseases</a>
              </p>
            </div>
  
            <div class="contact">
                <h6>Contact us</h6>
                <p class="footerLbl">
                    If you have any suggestions or feedback feel free to contact us at: info@simulationvirus.com
                </p>
            </div>
  
            <div class="disclaimer">
              <h6>Disclaimer</h6>
              <p class="footerLbl">
                  Although our simulation displays the general effect of viruses spreading around a population there are a lot of factors that are impossible to take into consideration with a simulation
              </p>
            </div>
        </div>

        <div class="containerFooterTwo">
            <div id="social">
                <ul class="social-icons">
                  <li><a class="linkedin" href="https://www.linkedin.com/in/harrison-moude-foko-a2b888114"><i class="fa fa-linkedin"></i></a></li>
                  <li><a class="linkedin" href="https://www.linkedin.com/in/floris-roos"><i class="fa fa-linkedin"></i></a></li>   
                </ul>
            </div>
            <div id="copyright">
                <p class="copyright-text">
                  Copyright &copy; 2020 All Rights Reserved by Floris and Harrison.
                </p>
            </div>
        </div>
    </footer>

</body>
</html>